'use strict';

function RestCRUD(addressServeur) {
    if (undefined === addressServeur) return null;
    var _VM = this;
    var _REST_ADR = addressServeur;
    this.lastResult = null;

    this.create = function(objRessource, ressource, clbk) {
        var xhr = new XMLHttpRequest();
        xhr.open('POST', _REST_ADR + ressource, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = _xhr.responseText;
            if (undefined != clbk) clbk(_xhr.responseText);
        };
        xhr.send(JSON.stringify(objRessource));
    };

    this.update = function(objRessource, ressource, clbk) {
        var xhr = new XMLHttpRequest();
        xhr.open('PUT', _REST_ADR + ressource, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = _xhr.responseText;
            if (undefined != clbk) clbk(_xhr.responseText);
        };
        xhr.send(JSON.stringify(objRessource));
    };

    this.read = function(ressource, clbk) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', _REST_ADR + ressource, true);
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = _xhr.responseText;
            if (undefined != clbk) clbk(_xhr.responseText);
        };
        xhr.send();
    };

    this.delete = function(ressource, clbk) {
        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', _REST_ADR + ressource, true);
        xhr.onreadystatechange = function(evt) {
            var _xhr = evt.target;
            if (_xhr.readyState < 4 || _xhr.status < 200 || _xhr.status >= 300) return;
            console.log(_xhr.response);
            this.lastResult = _xhr.responseText;
            if (undefined != clbk) clbk(_xhr.responseText);
        };
        xhr.send();
    };
}


// get('/notes', function(responseText) {
//     var myParseOfJson = JSON.parse(responseText);
//     //console.log(myParseOfJson);
//     myParseOfJson.map(elem => {
//         get('/users/' + elem.user, function(resp) {
//             var userObjResponse = JSON.parse(resp);
//             elem.user = userObjResponse.img;
//             elem.username = userObjResponse.name;
//             console.log(elem);
//             addNoteToList(elem);
//         });

//     });
//     //addNoteToList()
// });