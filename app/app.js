'use strict';

/*######################## Vérification du chargement du JS ########################*/
function jsLoaded() {
    var jsloaded = document.querySelector('#is-js-loading');
    jsloaded.style.display = 'none';
    initFormEditnote();
}
jsLoaded();

var isFormShown = true;

/*######################## Initialisation du formulaire ########################*/
function initFormEditnote() {
    var a = document.forms['form-edit-note'];
    a.addEventListener('submit', submitnote);
    a.addEventListener('reset', resetnote);
    document.querySelector('.hide-button').addEventListener('click', toggleEditForm);
    document.forms['form-edit-note'].querySelectorAll('input:not(.btn),textarea, select')
        .forEach((element) => {
            element.addEventListener('focus', function(evt) {
                console.log(evt);
                evt.target.classList.remove('invalid-input');
            });
        });
}

function submitnote(evt) {
    if (undefined == evt) return;
    evt.preventDefault();
    console.log(evt);
    var objNewNote = checkValidityOfForm();
    if (objNewNote == null) return;
    var img = objNewNote.userimg;
    delete(objNewNote.userimg);
    var name = objNewNote.username;
    delete(objNewNote.username);
    notes_restcrud.create(objNewNote, '/notes', function(response) {
        var noteComplete = JSON.parse(response);
        noteComplete.userimg = img;
        noteComplete.username = name;
        addNoteToList(noteComplete);
        document.forms['form-edit-note'].reset();
    });
}

/*######################## Masquer le contenu éditable ########################*/
function toggleEditForm() {
    isFormShown = !isFormShown;
    document.querySelector('.wrapper').style.display = (isFormShown) ? 'block' : 'none';
    document.querySelector('#edit-note').classList.toggle('mini');
}

/*######################## Reset du formulaire ########################*/
function resetnote(evt) {
    evt.preventDefault();
    //document.forms['form-edit-note'].reset();
    var nodes = document.forms['form-edit-note'].querySelectorAll('input:not(.btn),textarea, select')
        .forEach(function(element) { element.value = '' });
    document.forms['form-edit-note'].querySelectorAll('input:not(.btn),textarea, select')
        .forEach((elem) => { elem.classList.remove('invalid-input'); });

    document.forms['form-edit-note']['input-user'].selectedIndex = -1;

}

/*######################## Vérification de la validité du formulaire ########################*/
function checkValidityOfForm() {
    var valid = true;

    var titre = document.forms['form-edit-note']['input-titre'];
    if (titre.value.length <= 2) {
        valid = false;
        titre.classList.add('invalid-input');
    } else {
        titre.classList.remove('invalid-input');
    }

    var date = document.forms['form-edit-note']['input-date'];
    if (undefined == date.value || (String(date.value).length != 10 || Date.parse(date.value) == undefined)) {
        valid = false;
        date.classList.add('invalid-input');
    } else {
        date.classList.remove('invalid-input');
    }

    var time = document.forms['form-edit-note']['input-time'];
    const regex = /^([01]\d)|(2[0-3]):[0-5]\d$/;
    if (regex.exec(time.value) == null) {
        valid = false;
        time.classList.add('invalid-input');
    } else {
        time.classList.remove('invalid-input');
    }

    var description = document.forms['form-edit-note']['input-description'];
    if (description.value == '') {
        valid = false;
        description.classList.add('invalid-input');
    } else {
        description.classList.remove('invalid-input');
    }

    var user = document.forms['form-edit-note']['input-user'];
    var userName = '';
    var userid = '';
    if (-1 == user.selectIndex || user.value == '') {
        valid = false;
        user.classList.add('invalid-input');
    } else {
        user.classList.remove('invalid-input');
        var infosUser = user.options[user.selectedIndex]
            .innerHTML.split(':');

        userid = infosUser[0];
        userName = infosUser[1];
    }

    console.log(titre, date, time, user, description);

    return (valid) ? {
        titre: titre.value,
        date: date.value,
        time: time.value,
        description: description.value,
        userimg: user.value,
        username: userName,
        user: userid
    } : null;
}


/*######################## Ajout d'une note ########################*/
function addNoteToList(objNoteToAdd) {
    if (undefined === objNoteToAdd && ((objNoteToAdd = checkValidityOfForm()) == null)) return false;
    console.log(objNoteToAdd);

    var maDivNote = document.createElement('div');
    maDivNote.classList.add('note');
    maDivNote.id = 'note-' + objNoteToAdd.id;
    maDivNote.innerHTML = '<img class="img-close" src="img/close.png" alt="Fermer"><div class="note-user"><img src="img/' + objNoteToAdd.userimg + '" alt=""><br/>' + objNoteToAdd.user + ':' + objNoteToAdd.username + '</div><div class="note-right"><div class="note-titre">' + objNoteToAdd.titre + '</div><div class="note-datetime">' + objNoteToAdd.date + ' à ' + objNoteToAdd.time + '</div></div><div class="note-description">' + objNoteToAdd.description + '</div>';

    maDivNote.querySelector('.img-close').addEventListener('dblclick', deletenote);

    document.querySelector('#notes-container').appendChild(maDivNote);
    return true;
}


/*######################## Suppression d'une note ########################*/
function deletenote(evt) {
    console.log(evt.target);
    var isOkToDelete = confirm("Souhaitez vous vraiment supprimer cette note ?");
    if (isOkToDelete) {

        var idnote = evt.target.parentElement.id.split('-')[1];
        notes_restcrud.delete('/notes/' + idnote, function(response) {
            alert('Suppression OK');
            evt.target.parentElement.remove();
        });
    }
}


var notes_restcrud = new RestCRUD('http://localhost:7500');

notes_restcrud.read('/notes', function(responseText) {
    var myParseOfJson = JSON.parse(responseText);
    //console.log(myParseOfJson);
    myParseOfJson.map(elem => {
        notes_restcrud.read('/users/' + elem.user, function(resp) {
            var userObjResponse = JSON.parse(resp);
            elem.userimg = userObjResponse.img;
            elem.username = userObjResponse.name;
            console.log(elem);
            addNoteToList(elem);
        });

    });
    //addNoteToList()
});

var mymap = L.map('mapid').setView([51.505, -0.09], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
}).addTo(mymap);

var marker = L.marker([51.5, -0.09]).addTo(mymap);

var circle = L.circle([51.508, -0.11], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 500
}).addTo(mymap);

var polygon = L.polygon([
    [51.509, -0.08],
    [51.503, -0.06],
    [51.51, -0.047]
]).addTo(mymap);

var greenIcon = L.icon({
    iconUrl: 'img/close.png',

    iconSize: [40, 40], // size of the icon
    shadowSize: [50, 64], // size of the shadow
    iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [4, 62], // the same for the shadow
    popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
});

L.marker([51.5, -0.09], { icon: greenIcon }).addTo(mymap);