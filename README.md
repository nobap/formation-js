**Notes**

## Tools
- VS Code
    - Bootstrap 3 Snippet
    - ESLint
    - Git History
    - aAutoconfig
- NPM Lite Server
- [Json Server](https://github.com/typicode/json-server)
- Extension Chrome : Client RESTful Web Service

## Links
- [CSS Diner](https://flukeout.github.io/) 
- [Devdocs.io](https://devdocs.io/)
- [Regex](https://regex101.com/)
- [Json shema](https://json-schema.org/)
- [Json shema](https://jsonschema.net/)
- [Leafletjs] (https://leafletjs.com/)

## Other
- input pattern regex
- w3c plugin validation